# Minimalist example of web2py + pdf.js

## Items of Note

1. This is based off an old version of pdf.js and a javascript I found on the internet.  More information is included in the minimal_viewer.js
2. Licensing information for pdf.js is in the pdfjs directory.
3. It could definitely be improved by incorporating the new pdf.js