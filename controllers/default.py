# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    form = SQLFORM(db.user_pdf)
    
    if form.process().accepted:
        response.flash = 'PDF Uploaded'
        
    pdfs = db(db.user_pdf).select()
    
    return dict(form=form,pdfs=pdfs)

def view():
    pdf_id = request.args(0)
    response.files.append(URL('static','js/pdfjs/pdf.js'))
    response.files.append(URL('static','js/pdfjs/textlayerbuilder.js'))
    response.files.append(URL('static','js/pdfjs/minimal_viewer.js'))

    return dict(pdf_id=pdf_id)

def pdf_data():
    min_page = 1

    row_id = request.vars.id

    page = request.vars.page or min_page

    try:
        page = int(page)
    except:
        page = min_page

    try:
        next_page = page + 1
    except:
        next_page = min_page
    try:
        previous_page = max(page - 1, min_page)
    except:
        previous_page = min_page

    table = db.user_pdf
        
    row = table(row_id)

    (filename, stream) = table.pdf_file.retrieve(row.pdf_file)

    next_vars = {'id':row_id, 'page':next_page}
    prev_vars = {'id':row_id, 'page':previous_page}


    data = {'page':page,
            'data': stream.read().encode('base64').encode('ascii'),
            'next_page_url' : URL('pdf_data', vars=next_vars),
            'prev_page_url' : URL('pdf_data', vars=prev_vars)}

    return response.json(data)



def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
